<?php
require_once 'LaTribuneException.php';
require_once 'LaTribuneResponse.php';

class LaTribuneApi
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $secret;

    /**
     * @var string
     */
    private $host;

    private $lastRequest;

    /**
     * @var bool
     */
    private $ssl;

    /**
     * @var array
     */
    private $extraParams;

    /**
     * LaTribuneApi constructor.
     *
     * @param $key
     * @param $secret
     * @param string $host
     * @param bool $ssl
     * @param array $extraParams
     *
     * @throws \LaTribuneApiException
     */
    public function __construct($key, $secret, $host = 'api.latribune.fr', $ssl = false, array $extraParams = array())
    {
        if (!function_exists('curl_init')) {
            throw new LaTribuneApiException('La Tribune API Kit requires CURL extension');
        }

        if (!function_exists('json_decode')) {
            throw new LaTribuneApiException('La Tribune API Kit requires JSON extension');
        }

        if (!function_exists('hash') || !in_array('sha256', hash_algos())) {
            throw new LaTribuneApiException("The 'sha256' hash algorithm is not available on your PHP installation");
        }

        $this->key    = $key;
        $this->secret = $secret;
        $this->host   = $host;
        $this->ssl = $ssl;
        $this->extraParams = $extraParams;
    }

    /**
     * @return mixed
     */
    public function getLastRequest()
    {
        return $this->lastRequest;
    }

    /**
     * @param $id
     * @param array $with
     *
     * @return LaTribuneApiResponse
     */
    public function getContact($id, $with = array())
    {
        return $this->send('GET', "/contact/$id", array(
            'with' => implode(',', $with)
        ));
    }

    /**
     * @return LaTribuneApiResponse
     */
    public function getNewsletter()
    {
        return $this->send('GET', '/newsletter');
    }

    /**
     * @param array $filters
     * @param array|null $with
     *
     * @return LaTribuneApiResponse
     */
    public function getContacts(array $filters = array(), array $with = null)
    {
        $params = $filters;

        if (isset($with)) {
            $params['with'] = implode(',', $with);
        }

        return $this->send('GET', '/contacts', $params);
    }

    /**
     * @param $login
     * @param $password
     * @param null $connexionIp
     * @param array|null $with
     *
     * @return LaTribuneApiResponse
     */
    public function postContactLogin($login, $password, $connexionIp = null, array $with = null)
    {
        $params = array('login' => $login, 'password' => $password);

        if (isset($connexionIp)) {
            $params['connexion_ip'] = $connexionIp;
        }

        if (isset($with)) {
            $params['with'] = implode(',', $with);
        }

        return $this->send('POST', '/contact/login', $params);
    }

    /**
     * @param array $contact
     *
     * @return LaTribuneApiResponse
     */
    public function postContact(array $contact)
    {
        return $this->send('POST', '/contact', $contact);
    }

    /**
     * @param $id
     * @param array $contact
     *
     * @return LaTribuneApiResponse
     */
    public function putContact($id, array $contact)
    {
        return $this->send('PUT', "/contact/$id" , $contact);
    }

    /**
     * @param $contactId
     *
     * @return LaTribuneApiResponse
     */
    public function getContactPurchases($contactId)
    {
        return $this->send('GET', "/contact/$contactId/purchases");
    }

    /**
     * @param $contactId
     * @param array $purchase
     *
     * @return LaTribuneApiResponse
     */
    public function postContactPurchase($contactId, array $purchase)
    {
        return $this->send('POST', "/contact/$contactId/purchase", $purchase);
    }

    /**
     * @param $contactId
     *
     * @return LaTribuneApiResponse
     */
    public function getContactSubscriptions($contactId)
    {
        return $this->send('GET', "/contact/$contactId/subscriptions");
    }

    /**
     * @param $subscriptionId
     * @param array $subscription
     *
     * @return LaTribuneApiResponse
     */
    public function putSubscription($subscriptionId, array $subscription)
    {
        return $this->send('PUT', "/subscription/$subscriptionId", $subscription);
    }

    /**
     * @param $contactId
     * @param array $subscription
     *
     * @return LaTribuneApiResponse
     */
    public function postContactSubscription($contactId, array $subscription)
    {
        return $this->send('POST', "/contact/$contactId/subscription", $subscription);
    }

    /**
     * @return LaTribuneApiResponse
     */
    public function getJobs()
    {
        return $this->send('GET', '/jobs');
    }

    /**
     * @return LaTribuneApiResponse
     */
    public function getCountries()
    {
        return $this->send('GET', '/countries');
    }

    /**
     * @return LaTribuneApiResponse
     */
    public function getCategories()
    {
        return $this->send('GET', '/categories');
    }

    /**
     * @param null $depth
     *
     * @return LaTribuneApiResponse
     */
    public function getCategoriesTree($depth = null)
    {
        $params = $depth ? array('depth' => $depth) : array();

        return $this->send('GET', '/categories/tree', $params);
    }

    /**
     * @param $categoryId
     * @param int $page
     *
     * @return LaTribuneApiResponse
     */
    public function getCategoryArticles($categoryId, $page = 0)
    {
        $params = isset($page) ? array('page' => $page) : array();

        return $this->send('GET', "/category/$categoryId/articles", $params);
    }

    /**
     * @param $categoryId
     *
     * @return LaTribuneApiResponse
     */
    public function getCategoryArticlesLastUpdate($categoryId)
    {
        return $this->send('GET', "/category/$categoryId/articles/lastupdate");
    }

    /**
     * @param $categoryId
     * @param bool $withDiaporama
     * @param int $limit
     * @param bool $withProtected
     *
     * @return LaTribuneApiResponse
     */
    public function getCategoryHomepage($categoryId, $withDiaporama = false, $limit = 120, $withProtected = false)
    {
        $params = array('diaporama' => $withDiaporama, 'limit' => $limit, 'protected' => $withProtected);

        return $this->send('GET', "/category/$categoryId/homepage", $params);
    }

    /**
     * @param $categoryId
     *
     * @return LaTribuneApiResponse
     */
    public function getCategoryHomepageLastUpdate($categoryId)
    {
        return $this->send('GET', "/category/$categoryId/homepage/lastupdate");
    }

    /**
     * @return LaTribuneApiResponse
     */
    public function getRelevantInformations()
    {
        return $this->send('GET', '/relevant_information');
    }

    /**
     * @param $id
     *
     * @return LaTribuneApiResponse
     */
    public function getArticle($id)
    {
        return $this->send('GET', "/article/$id");
    }

    /**
     * @param $id
     *
     * @return LaTribuneApiResponse
     */
    public function getArticleComments($id)
    {
        return $this->send('GET', "/article/$id/comments");
    }

    /**
     * @param $articleId
     * @param array $comment
     *
     * @return LaTribuneApiResponse
     */
    public function postArticleComment($articleId, array $comment)
    {
        return $this->send('POST', "/article/$articleId/comment", $comment);
    }

    /**
     * @param $query
     *
     * @return LaTribuneApiResponse
     */
    public function getSearch($query)
    {
        return $this->send('GET', '/search', array('q' => $query));
    }



    /**
     * Méthodes dépréciées qui ne doivent plus être utilisées
     * À remplacer par celle indiquée dans le commentaire @deprecated qui se trouve juste au dessus
     */

    /**
     * @deprecated use LaTribuneApi->getContact();
     *
     * @param $id
     * @param array $with
     *
     * @return LaTribuneApiResponse
     */
    public function getMember($id, $with = array())
    {
        return $this->send('GET', "/member/$id", array(
                'with' => implode(',', $with)
            ));
    }

    /**
     * @deprecated use LaTribuneApi->getContacts();
     *
     * @param array $filters
     * @param array $with
     *
     * @return LaTribuneApiResponse
     */
    public function getMembers(array $filters = array(), array $with = null)
    {
        $params = $filters;

        if (isset($with)) {
            $params['with'] = implode(',', $with);
        }

        return $this->send('GET', '/members', $params);
    }

    /**
     * @deprecated use LaTribuneApi->postContactLogin();
     *
     * @param $login
     * @param $password
     * @param null $connexionIp
     * @param array $with
     *
     * @return LaTribuneApiResponse
     */
    public function postLogin($login, $password, $connexionIp = null, array $with = null)
    {
        $params = array('login' => $login, 'password' => $password);

        if (isset($connexionIp)) {
            $params['connexion_ip'] = $connexionIp;
        }

        if (isset($with)) {
            $params['with'] = implode(',', $with);
        }

        return $this->send('POST', '/login', $params);
    }

    /**
     * @deprecated use LaTribuneApi->postContact();
     *
     * @param array $member
     *
     * @return LaTribuneApiResponse
     */
    public function postMember(array $member)
    {
        return $this->send('POST', '/member', $member);
    }

    public function getMostReadArticle()
    {
        return $this->send('GET', '/article/mostread');
    }

    public function getDailyEdition()
    {
        return $this->send('GET', '/edition/daily');
    }

    public function getWeeklyEdition()
    {
        return $this->send('GET', '/edition/weekly');
    }

    /**
     * @return LaTribuneApiResponse
     */
    public function getRdv()
    {
        return $this->send('GET', '/rendezvous');
    }

    /**
     * @return LaTribuneApiResponse
     */
    public function getBourse()
    {
        return $this->send('GET', '/bourse/bar');
    }

    /**
     * @return LaTribuneApiResponse
     */
    public function getLive()
    {
        return $this->send('GET', '/live');
    }

    /**
     * @return LaTribuneApiResponse
     */
    public function getLiveLastUpdate()
    {
        return $this->send('GET', '/live/lastupdate');
    }

    /**
     * @deprecated use LaTribuneApi->putContact();
     *
     * @param $id
     * @param array $member
     *
     * @return LaTribuneApiResponse
     */
    public function putMember($id, array $member)
    {
        return $this->send('PUT', "/member/$id" , $member);
    }

    /**
     * @deprecated use LaTribuneApi->postContactSubscription();
     *
     * @param $memberId
     * @param array $subscription
     *
     * @return LaTribuneApiResponse
     */
    public function postSubscription($memberId, array $subscription)
    {
        return $this->send('POST', "/member/$memberId/subscription", $subscription);
    }

    /**
     * @deprecated use LaTribuneApi->getContactSubscriptions();
     *
     * @param $memberId
     *
     * @return LaTribuneApiResponse
     */
    public function getMemberSubscriptions($memberId)
    {
        return $this->send('GET', "/member/$memberId/subscriptions");
    }

    /**
     * Internals
     *
     * @param $method
     * @param $resource
     * @param array $params
     *
     * @return LaTribuneApiResponse
     */
    private function send($method, $resource, array $params = array())
    {
        $params = array_merge($params, $this->extraParams);
        array_walk($params, array($this, 'normalizeParam'));
        $params['api_key'] = $this->key;
        $params['api_time'] = time();
        $params['api_signature'] = $this->computeSignature($resource, $params);
        $url = 'http'.($this->ssl ? 's' : '').'://' . $this->host . $resource;

        $this->lastRequest = array(
            'method' => $method,
            'url' => $url,
            'params' => $params
        );

        list($json, $code) = $this->doCurl($method, $url, $params);

        $this->lastRequest['response_code'] = $code;
        $this->lastRequest['response_body'] = $json;

        return new LaTribuneApiResponse($json, $code);
    }

    private function normalizeParam(&$param)
    {
        if (is_bool($param)) {
            $param = (int)$param;
        }

        return $param;
    }

    private function computeSignature($resource, $params)
    {
        $params['url'] = $resource;
        $params['api_secret'] = $this->secret;
        ksort($params);
        return hash('sha256', implode('', $params));
    }

    private function doCurl($method, $url, array $params)
    {
        $headers = array(
            'Accept: application/json'
        );

        $curly = curl_init();

        if ($method === 'GET' || $method === 'DELETE') {
            $url .= '?' . http_build_query($params);
            $this->lastRequest['url'] = $url;
        } else {
            curl_setopt($curly, CURLOPT_POSTFIELDS, http_build_query($params));
        }

        curl_setopt($curly, CURLOPT_HEADER, 0);
        curl_setopt($curly, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curly, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curly, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curly, CURLOPT_URL, $url);
        curl_setopt($curly, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curly, CURLOPT_SSL_VERIFYHOST, false);
        $responseBody = curl_exec($curly);
        $responseCode = curl_getinfo($curly, CURLINFO_HTTP_CODE);

        if ($responseBody === false) {
            $e = new LaTribuneApiException(sprintf('Curl error #%s : %s', curl_error($curly), curl_errno($curly)));
            curl_close($curly);
            throw $e;
        }

        curl_close($curly);

        return array($responseBody, $responseCode);
    }
}
