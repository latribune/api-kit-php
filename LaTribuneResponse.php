<?php

class LaTribuneApiResponse implements IteratorAggregate
{
    private $data;
    private $httpCode;

    function __construct($response, $httpCode)
    {
        $this->data = json_decode($response, true);
        $this->httpCode = (int)$httpCode;

        if (is_null($response)) {
            throw new LaTribuneApiException("Cannot decode API JSON response");
        }
    }

    public function isError()
    {
        return $this->httpCode >= 400;
    }

    public function isClientError()
    {
        return $this->httpCode >= 400 && $this->httpCode < 500;
    }

    public function isServerError()
    {
        return $this->httpCode >= 500;
    }

    public function isSuccess()
    {
        return $this->httpCode == 200;
    }

    public function getErrors()
    {
        return isset($this->data['_errors']) ? $this->data['_errors'] : null;
    }

    public function getData()
    {
        return $this->data;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     */
    public function getIterator()
    {
        return new ArrayIterator($this->data);
    }
}
