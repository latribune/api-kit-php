# README #

Ce Readme est destiné à compléter la documentation existante de l'API (au format PDF)

### Ajout d'un compte API ###

* Contexte

Les dernières évolutions de l'API consistent par exemple en l'utilisation exclusive de la base de données pour gérer la configuration de l'API.
Ainsi, on évite une mise en production pour la simple création d'un compte.

* Configuration

Auparavant, une partie de la conf était décrite dans app/config/la_tribune_api.yml : on garde ce fichier à titre indicatif, au cas où.
Il faut désormais appliquer directement des inserts en base de données dans deux tables.

* Database configuration

Les deux tables impliquées dans la base de données, pour la création d'un compte API, sont **applications**, et **applications_routes**.

Dans **applications** : on ajoute une ligne pour ajouter le nom, le debug si besoin, la clé secrète, et un identifiant de site.

Dans **applications_routes** : on ajoute pour chaque ligne une route avec l'identifiant qui s'y rapporte, ainsi que l'identifiant correspondant au compte API correspondant (à ne pas confondre avec l'identifiant de site).